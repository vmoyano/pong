package juego;

import java.awt.Color;

import entorno.Entorno;

public class Slider {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	
	public Slider(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 200;
		this.alto = 40;
		this.velocidad = 2;
	}
	
	public void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.WHITE);
	}

	public void moverDerecha(Entorno e) {
		if(this.x + this.ancho/2 + this.velocidad < e.ancho()) {
		     this.x += velocidad;
	    }
	}
	
	public void moverIzquierda() {
		if(this.x - this.ancho/2 > 0) {
		    this.x -= velocidad;
		}
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getAncho() {
		return this.ancho;
	}
	
	public double getAlto() {
		return this.alto;
	}

}
